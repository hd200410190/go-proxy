package main

import (
    "os"
    "fmt"
    "log"
    "net/http"
    "net/http/httputil"
    "net/url"
    "flag"
    "regexp"
    "github.com/julienschmidt/httprouter"
)

var webServer string
var fileServer string
var webURL *url.URL
var fileURL *url.URL
var webProxy *httputil.ReverseProxy
var fileProxy *httputil.ReverseProxy

func GeneralRequestHandle(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
    requestStr := ps.ByName("request")

    matched, err := regexp.MatchString(`^/file/`, requestStr)

    if err != nil {
        fmt.Println(err)
        return
    }
    
    // file request
    if matched {
        fmt.Println("DEBUG: Requested file", requestStr)
        redirectFileRequest(w, r)
    // other request - web request
    } else {
        fmt.Println("DEBUG: Requested web", requestStr)
        redirectWebRequest(w, r)
    }
}

func redirectWebRequest(w http.ResponseWriter, r *http.Request) {
    fmt.Println("DEBUG: Web request redirected")

	// Update the headers to allow for SSL redirection
	r.URL.Host = webURL.Host
	r.URL.Scheme = webURL.Scheme
	r.Header.Set("X-Origin-Host", webURL.Host)
	r.Header.Set("X-Forwarded-Host", r.Header.Get("Host"))
	r.Host = webURL.Host

	// Note that ServeHttp is non blocking and uses a go routine under the hood
	webProxy.ServeHTTP(w, r)
}

func redirectFileRequest(w http.ResponseWriter, r *http.Request) {
    fmt.Println("DEBUG: File request redirected")

	// Update the headers to allow for SSL redirection
	r.URL.Host = fileURL.Host
	r.URL.Scheme = fileURL.Scheme
	r.Header.Set("X-Origin-Host", fileURL.Host)
	r.Header.Set("X-Forwarded-Host", r.Header.Get("Host"))
	r.Host = fileURL.Host

	// Note that ServeHttp is non blocking and uses a go routine under the hood
	fileProxy.ServeHTTP(w, r)
}

func main() {
    // get the port number (default: 80)
    portPrt := flag.Int("port", 80, "Port Number")
    flag.StringVar(&webServer, "web", "NOT SET", "web request host")
    flag.StringVar(&fileServer, "file", "NOT SET", "file request host")

    flag.Parse()

    if webServer == "NOT SET" {
        fmt.Println("ERROR: Web server not set")
        os.Exit(1)
    }

    if fileServer == "NOT SET" {
        fmt.Println("ERROR: File server not set")
        os.Exit(1)
    }

    // parse the url
    webURL, _ = url.Parse(webServer)
    webProxy = httputil.NewSingleHostReverseProxy(webURL)

    fileURL, _ = url.Parse(fileServer)
    fileProxy = httputil.NewSingleHostReverseProxy(fileURL)

    // instantiate router
    router := httprouter.New()

    // general requests handler
    router.GET("/*request", GeneralRequestHandle)
    router.POST("/*request", GeneralRequestHandle)
    router.PUT("/*request", GeneralRequestHandle)
    router.DELETE("/*request", GeneralRequestHandle)

    // bind router to port
    log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", *portPrt), router))
}
